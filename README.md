# Changelog
A tool for generating a CHANGELOG.md according to [keepachangelog.com](https://keepachangelog.com/en/1.1.0/) from a collection of yaml files each representing a single changelog entry.
Inspired by the GitLab team's workflow:
- https://docs.gitlab.com/ee/development/changelog.html
- https://gitlab.com/gitlab-org/gitlab-foss/blob/master/bin/changelog, and
- https://gitlab.com/gitlab-org/release-tools/tree/a025bfc59ef32d4b20250442a4093ac42cb4e6f1/lib/release_tools/changelog

## Development
This is written in Rust. Check your distribution's instructions on how to install Rust.

Please also install and use [rustfmt](https://github.com/rust-lang/rustfmt) to format your code appropriately.  
Using the [rust-lang/rust.vim](https://github.com/rust-lang/rust.vim) plugin, vim does that for you.

Everything else you need are `cargo` commands.

## Usage
This tool expects two command line arguments:
1. The path to your changelog entry collection (directory with your yaml files)
2. The path to which to write the resulting CHANGELOG.md

### Meta file
- You can optionally include a meta file (in the changelog-entries' directory)
- The name of the file must follow this scheme: `meta.y[a]ml`
- Currently supported meta-arguments: `header`
- Its content should look like this:
```yaml
header: |
        Some description.
        Another newline.
```

### Changelog entry
Note: This is closely based on https://docs.gitlab.com/ee/development/changelog.html#overview

A changelog entry file should be named like this:
`<version>-<unique name>.yml` - e.g. `0.1.0-document-usage.yml`.

`<version>` is the released version of your project. This is used as the version heading in the generated CHANGELOG.md.  
It has to follow this scheme: `major.minor.patch`, as described in [Semantic Versioning](https://semver.org/).  
Prerelease versions like `1.0.0-beta1` are currently not supported.

`<unique name>` is just for you to know what's in the entry, and to keep the filenames unique. This is _not_ used in the resulting markdown.

The content should look like this:

```yaml
---

title: "Document changelog usage"
type: added
merge_request: 42
issue: 1337
```

The `title` and `type` fields are **mandatory**.  
`title` is the change that will be added to your CHANGELOG.md.  
`type` can be one of:
- added
- changed
- fixed
- removed

Note: This doesn't include [all possible types](https://keepachangelog.com/en/1.1.0/#how); this may change depending on our needs.

`merge_request` and `issue` are **optional**.
When given, links to the merge request or issue are added to the changelog entry.
