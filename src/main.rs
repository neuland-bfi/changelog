use clap::Parser;
use std::fs::write;

use changelog::{Changelog, Markdown};

#[derive(Parser, Debug)]
#[clap(version, about)]
struct Args {
    /// Path to the directory containing the changelog entry files
    entries_dir: String,
    /// Path to the CHANGELOG.md file to be generated. Pass '-' to print to stdout.
    changelog_file: String,
}

fn main() {
    let args = Args::parse();

    // safe unwraps due to clap's Arg.required
    let entries_dir = args.entries_dir;
    let output_file = args.changelog_file;

    match Changelog::from_entries_dir(&entries_dir) {
        Ok(c) => {
            if output_file == "-" {
                println!("{}", c.markdown())
            } else {
                match write(output_file, c.markdown()) {
                    Ok(_) => {}
                    Err(e) => eprintln!("Error writing file: {}", e),
                }
            }
        }
        Err(e) => eprintln!("Error generating changelog: {}", e),
    }
}
