use std::error::Error as StdError;
use std::fmt;
use std::io::Error as IOError;
use std::io::ErrorKind as IOErrorKind;

use semver::Error as SemVerError;
use serde_yaml::Error as YamlError;

#[derive(Debug, PartialEq)]
pub enum ChangeError {
    NotFound,
    PermissionDenied,
    OtherError,
    InvalidData,
    InvalidYaml(String),
    InvalidVersion(String),
}

impl fmt::Display for ChangeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ChangeError::NotFound => f.write_str("NotFound"),
            ChangeError::PermissionDenied => f.write_str("PermissionDenied"),
            ChangeError::InvalidData => f.write_str("InvalidData"),
            ChangeError::OtherError => f.write_str("OtherError"),
            ChangeError::InvalidYaml(ye) => write!(f, "Invalid yaml: {}", ye),
            ChangeError::InvalidVersion(filename) => {
                write!(f, "Invalid version in filename: {}", filename)
            }
        }
    }
}

impl StdError for ChangeError {
    fn description(&self) -> &str {
        match self {
            ChangeError::NotFound => "File not found!",
            ChangeError::PermissionDenied => "Permission denied!",
            ChangeError::InvalidData => "File's contents must be valid UTF-8!",
            ChangeError::OtherError => {
                "Some of the directory components was not actually a directory!"
            }
            ChangeError::InvalidYaml(ye) => &ye,
            ChangeError::InvalidVersion(filename) => &filename,
        }
    }
}

impl From<IOError> for ChangeError {
    fn from(e: IOError) -> Self {
        match e.kind() {
            IOErrorKind::NotFound => ChangeError::NotFound,
            IOErrorKind::PermissionDenied => ChangeError::PermissionDenied,
            IOErrorKind::InvalidData => ChangeError::InvalidData,
            _ => ChangeError::OtherError,
        }
    }
}

impl From<YamlError> for ChangeError {
    fn from(e: YamlError) -> Self {
        ChangeError::InvalidYaml(e.to_string())
    }
}

impl From<SemVerError> for ChangeError {
    fn from(e: SemVerError) -> Self {
        ChangeError::InvalidVersion(e.to_string())
    }
}
