use crate::changelog::{Change, Changelog, Meta, Release};
#[cfg(test)]
use crate::types::ChangeType;

#[cfg(test)]
use semver::Version;

pub trait Markdown {
    fn markdown(&self) -> String;
}

impl Markdown for Change {
    fn markdown(&self) -> String {
        // always begin the change as a markdown list with the title
        let mut md = format!("- {}", self.title());
        //
        if let Some(issue) = self.issue() {
            md = format!("{} (#{}", md, issue); // parantheses are closed later
        };
        if let Some(merge_request) = self.merge_request() {
            // parantheses are closed later
            md = match self.issue() {
                // Some means parantheses were already opened and contain the issue reference
                Some(_) => format!("{}, !{}", md, merge_request),
                // None means no issue, so no opened parantheses yet
                None => format!("{} (!{}", md, merge_request),
            };
        };
        // always close parantheses if there are any references to an issue or merge request
        if self.issue().is_some() || self.merge_request().is_some() {
            md = format!("{})", md);
        };

        md
    }
}

impl Markdown for Release {
    fn markdown(&self) -> String {
        let version = match &self.version() {
            Some(v) => v.to_string(),
            None => String::from("Unreleased"),
        };
        let mut md = format!("## {}", version);
        for changetype in self.change_types() {
            if Some(&changetype) == self.change_types().first() {
                md = format!("{}\n### {}", md, changetype);
            } else {
                md = format!("{}\n\n### {}", md, changetype);
            }

            for change in self
                .changes()
                .into_iter()
                .filter(|c| c.change_type() == &changetype)
            {
                md = format!("{}\n{}", md, change.markdown());
            }
        }

        md
    }
}

impl Markdown for Changelog {
    fn markdown(&self) -> String {
        let meta = self.meta();
        let mut md = String::from("# Changelog\n");
        md.push_str(&meta.markdown());
        if meta.has_header() {
            md.push_str("\n\n");
        }
        for release in self.releases() {
            md.push_str(&release.markdown());
            if Some(release) != self.releases().last() {
                md.push_str("\n\n");
            }
        }

        md
    }
}

impl Markdown for Meta {
    fn markdown(&self) -> String {
        format!("{}", self.header())
    }
}

#[cfg(test)]
mod test_markdown_for_change {
    use super::*;

    #[test]
    fn should_always_contain_change_title_but_no_reference_if_none_given() {
        //! no change type here as that's used for the heading in Release
        let change = Change::new("Tool now does new thing", ChangeType::Added, None, None);
        let expected = String::from("- Tool now does new thing");

        assert_eq!(change.markdown(), expected);
    }

    #[test]
    fn should_refer_to_issue_if_given() {
        let change = Change::new("Tool now does new thing", ChangeType::Added, Some(42), None);
        let expected = String::from("- Tool now does new thing (#42)");

        assert_eq!(change.markdown(), expected);
    }

    #[test]
    fn should_refer_to_merge_request_if_given() {
        let change = Change::new(
            "Tool now does new thing",
            ChangeType::Added,
            None,
            Some(9000),
        );
        let expected = String::from("- Tool now does new thing (!9000)");

        assert_eq!(change.markdown(), expected);
    }

    #[test]
    fn should_refer_to_both_merge_request_and_issue_if_both_given() {
        let change = Change::new(
            "Tool now does new thing",
            ChangeType::Added,
            Some(42),
            Some(9000),
        );
        let expected = String::from("- Tool now does new thing (#42, !9000)");

        assert_eq!(change.markdown(), expected);
    }
}

#[cfg(test)]
mod test_markdown_for_release {
    use super::*;

    #[test]
    fn should_contain_headings_for_all_change_types_used_in_release() {
        let release = Release::new(
            vec![
                Change::new("New feature", ChangeType::Added, None, None),
                Change::new("Removed feature", ChangeType::Removed, None, None),
                Change::new("Changed feature", ChangeType::Changed, None, None),
                Change::new("Fixed bug", ChangeType::Fixed, None, None),
            ],
            Some(Version::new(1, 0, 0)),
        );
        let markdown = release.markdown();
        let expected = r#"## 1.0.0
### Added
- New feature

### Changed
- Changed feature

### Fixed
- Fixed bug

### Removed
- Removed feature"#;

        assert_eq!(markdown, expected);
    }

    #[test]
    fn should_not_have_empty_changetype_sections() {
        let release = Release::new(
            vec![Change::new("Only change", ChangeType::Changed, None, None)],
            Some(Version::new(1, 0, 0)),
        );
        let markdown = release.markdown();
        let expected = r"## 1.0.0
### Changed
- Only change";

        assert_eq!(markdown, expected);
    }

    #[test]
    fn should_have_unreleased_as_heading_when_version_is_none() {
        let release = Release::new(
            vec![
                Change::new("New feature", ChangeType::Added, None, None),
                Change::new("Removed feature", ChangeType::Removed, None, None),
                Change::new("Changed feature", ChangeType::Changed, None, None),
                Change::new("Fixed bug", ChangeType::Fixed, None, None),
            ],
            None,
        );
        let markdown = release.markdown();
        let expected = r#"## Unreleased
### Added
- New feature

### Changed
- Changed feature

### Fixed
- Fixed bug

### Removed
- Removed feature"#;

        assert_eq!(markdown, expected);
    }
}

#[cfg(test)]
mod test_markdown_for_changelog {
    use super::*;

    #[test]
    fn should_contain_all_releases_and_changelog_heading_with_empty_meta() {
        let release1 = Release::new(
            vec![
                Change::new("New feature1", ChangeType::Added, None, None),
                Change::new("Removed feature1", ChangeType::Removed, None, None),
                Change::new("Changed feature1", ChangeType::Changed, None, None),
                Change::new("Fixed bug1", ChangeType::Fixed, None, None),
            ],
            Some(Version::new(1, 0, 0)),
        );
        let release2 = Release::new(
            vec![
                Change::new("New feature2", ChangeType::Added, None, None),
                Change::new("Removed feature2", ChangeType::Removed, None, None),
                Change::new("Changed feature2", ChangeType::Changed, None, None),
                Change::new("Fixed bug2", ChangeType::Fixed, None, None),
            ],
            Some(Version::new(2, 0, 0)),
        );
        let changelog = Changelog::new(Meta::new(String::new()), vec![release1, release2]);

        let result_md = changelog.markdown();
        let expected_md = r#"# Changelog
## 1.0.0
### Added
- New feature1

### Changed
- Changed feature1

### Fixed
- Fixed bug1

### Removed
- Removed feature1

## 2.0.0
### Added
- New feature2

### Changed
- Changed feature2

### Fixed
- Fixed bug2

### Removed
- Removed feature2"#;

        assert_eq!(expected_md, result_md, "Should contain all releases!")
    }

    #[test]
    fn should_contain_all_releases_and_changelog_heading_with_matching_meta() {
        let meta = Meta::new(String::from("This is a test header"));
        let release1 = Release::new(
            vec![
                Change::new("New feature1", ChangeType::Added, None, None),
                Change::new("Removed feature1", ChangeType::Removed, None, None),
                Change::new("Changed feature1", ChangeType::Changed, None, None),
                Change::new("Fixed bug1", ChangeType::Fixed, None, None),
            ],
            Some(Version::new(1, 0, 0)),
        );
        let release2 = Release::new(
            vec![
                Change::new("New feature2", ChangeType::Added, None, None),
                Change::new("Removed feature2", ChangeType::Removed, None, None),
                Change::new("Changed feature2", ChangeType::Changed, None, None),
                Change::new("Fixed bug2", ChangeType::Fixed, None, None),
            ],
            Some(Version::new(2, 0, 0)),
        );
        let unreleased = Release::new(
            vec![Change::new("Fix", ChangeType::Fixed, None, None)],
            None,
        );
        let changelog = Changelog::new(meta, vec![unreleased, release1, release2]);

        let result_md = changelog.markdown();
        let expected_md = r#"# Changelog
This is a test header

## Unreleased
### Fixed
- Fix

## 1.0.0
### Added
- New feature1

### Changed
- Changed feature1

### Fixed
- Fixed bug1

### Removed
- Removed feature1

## 2.0.0
### Added
- New feature2

### Changed
- Changed feature2

### Fixed
- Fixed bug2

### Removed
- Removed feature2"#;

        assert_eq!(expected_md, result_md, "Should contain all releases!")
    }
}

#[cfg(test)]
mod test_markdown_for_meta {
    use super::*;

    #[test]
    fn should_match_header() {
        let meta = Meta::new(String::from("test header"));
        let expected_md = String::from("test header");

        assert_eq!(meta.markdown(), expected_md, "Should match header");
    }
}
