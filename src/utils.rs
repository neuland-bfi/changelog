use std::fs::read_dir;
use std::io::Error as IoError;
use std::path::Path;

use lazy_static::lazy_static;
use regex::Regex;
use semver::Version;

use crate::error::ChangeError;

static UNRELEASED_FILENAME_PREFIX: &str = "unreleased";

lazy_static! {
    // this matches what is considered valid by `semver::Version`: major.minor.patch
    // and unreleased for changes not yet part of a release
    // both with yaml and yml as allowed file types
    static ref CHANGE_ENTRY_FILENAME_REGEX: Regex =
        Regex::new(r"^((\d+\.\d+\.\d+)|unreleased)-.+\.ya?ml").unwrap();
    static ref META_FILENAME_REGEX: Regex = Regex::new(r"^meta\.ya?ml").unwrap();
}

pub fn collect_entry_files(directory: &str) -> Result<Vec<String>, IoError> {
    let mut entry_files: Vec<String> = Vec::new();
    for file in read_dir(directory)? {
        let file = file?; // propagating this seems like a better idea than maybe missing some files in the real world
        if file.path().is_dir() {
            continue;
        }
        match file.path().to_str() {
            Some(s) => {
                if filename_matches_change_scheme(file.file_name().to_str().unwrap_or("")) {
                    entry_files.push(String::from(s));
                }
            }
            None => continue,
        }
    }
    entry_files.sort();
    Ok(entry_files)
}

pub fn version_from_filename(filename: &str) -> Result<Option<Version>, ChangeError> {
    let basename = match Path::new(filename).file_name() {
        Some(bn) => match bn.to_str() {
            Some(s) => s,
            None => return Err(ChangeError::OtherError),
        },
        None => return Err(ChangeError::OtherError),
    };
    let version_match: String = match CHANGE_ENTRY_FILENAME_REGEX.captures(basename) {
        Some(captures) => captures
            .get(1)
            .map_or(String::new(), |group| String::from(group.as_str())),
        None => return Err(ChangeError::InvalidVersion(String::from(basename))),
    };
    if version_match == UNRELEASED_FILENAME_PREFIX {
        Ok(None)
    } else {
        Ok(Some(Version::parse(&version_match)?))
    }
}

fn filename_matches_change_scheme(filename: &str) -> bool {
    CHANGE_ENTRY_FILENAME_REGEX.is_match(filename)
}

pub fn filename_matches_meta_scheme(filename: &str) -> bool {
    META_FILENAME_REGEX.is_match(filename)
}

#[cfg(test)]
pub mod test_utils {
    //! Utility functions to be used in tests
    use std::fs::{create_dir_all, remove_dir, remove_file, write};

    pub fn setup_temp_dir(directory: &str, files: Vec<&str>) {
        create_dir_all(directory).unwrap();
        for file in files {
            write(temp_file_path(directory, file), "content irrelevant").unwrap();
        }
    }

    pub fn cleanup_temp_dir(directory: &str, files: Vec<&str>) {
        for file in files {
            remove_file(temp_file_path(directory, file)).unwrap();
        }
        remove_dir(directory).unwrap();
    }

    pub fn temp_file_path(directory: &str, basename: &str) -> String {
        format!("{}/{}", directory, basename)
    }
}

#[cfg(test)]
mod test_filename_matches_change_scheme {
    use super::*;

    #[test]
    fn should_recognize_filename_starting_with_release_number() {
        assert!(filename_matches_change_scheme("0.1.0-foobar.yml"));
        assert!(filename_matches_change_scheme("0.2.0-foobar.yml"));
        assert!(filename_matches_change_scheme("1.0.0-foobar.yml"));
        assert!(filename_matches_change_scheme("1.0.0-foobar.yml"));
        assert!(filename_matches_change_scheme("2.42.1-foobar.yml"));
    }

    #[test]
    fn should_recognize_filename_starting_with_unreleased() {
        assert!(filename_matches_change_scheme("unreleased-foobar.yml"));
    }

    #[test]
    fn should_recognize_both_yml_and_yaml() {
        assert!(filename_matches_change_scheme("1.0.0-foobar.yml"));
        assert!(filename_matches_change_scheme("1.0.0-foobar.yaml"));
    }

    #[test]
    fn should_recognize_invalid_filenames() {
        assert!(!filename_matches_change_scheme("foo-bar.yml"));
        assert!(!filename_matches_change_scheme(".0-foobar.yaml"));
        assert!(!filename_matches_change_scheme("foo-bar"));
        // seemingly valid versions, but considered invalid by semver
        assert!(!filename_matches_change_scheme("1-foobar.yml"));
        assert!(!filename_matches_change_scheme("1.0-foobar.yml"));
    }
}

#[cfg(test)]
mod test_version_from_filename {
    use super::*;

    #[test]
    fn should_extract_version_from_valid_filenames() {
        assert_eq!(
            version_from_filename("0.1.0-foobar.yml"),
            Ok(Some(Version::new(0, 1, 0)))
        );
        assert_eq!(
            version_from_filename("0.2.0-foobar.yml"),
            Ok(Some(Version::new(0, 2, 0)))
        );
        assert_eq!(
            version_from_filename("1.0.0-foobar.yml"),
            Ok(Some(Version::new(1, 0, 0)))
        );
        assert_eq!(
            version_from_filename("1.0.0-foobar.yml"),
            Ok(Some(Version::new(1, 0, 0)))
        );
        assert_eq!(
            version_from_filename("2.42.1-foobar.yml"),
            Ok(Some(Version::new(2, 42, 1)))
        );
    }

    #[test]
    fn should_extract_version_from_absolute_filenames() {
        assert_eq!(
            version_from_filename("/foo/0.1.0-foobar.yml"),
            Ok(Some(Version::new(0, 1, 0)))
        );
        assert_eq!(
            version_from_filename("/foo/0.2.0-foobar.yml"),
            Ok(Some(Version::new(0, 2, 0)))
        );
        assert_eq!(
            version_from_filename("/foo/1.0.0-foobar.yml"),
            Ok(Some(Version::new(1, 0, 0)))
        );
        assert_eq!(
            version_from_filename("/foo/1.0.0-foobar.yml"),
            Ok(Some(Version::new(1, 0, 0)))
        );
        assert_eq!(
            version_from_filename("/foo/2.42.1-foobar.yml"),
            Ok(Some(Version::new(2, 42, 1)))
        );
    }

    #[test]
    fn should_extract_unreleased_as_none() {
        assert_eq!(version_from_filename("unreleased-foobar.yml"), Ok(None));
        assert_eq!(
            version_from_filename("/foo/unreleased-foobar.yml"),
            Ok(None)
        );
    }

    #[test]
    fn should_return_err_for_invalid_filenames() {
        assert!(version_from_filename("foobar").is_err());
        assert!(version_from_filename("foo.bar").is_err());
        assert!(version_from_filename(".yaml").is_err());
        assert!(version_from_filename("foo-bar.yaml").is_err());
    }
}

#[cfg(test)]
mod test_collect_entry_files {
    use super::*;
    use test_utils::{cleanup_temp_dir, setup_temp_dir, temp_file_path};

    #[test]
    fn should_find_all_yaml_and_yml_files_with_change_naming_scheme() {
        let temp_dir = "/tmp/changelog_utils_collect_should_find_relevant";
        setup_temp_dir(
            temp_dir,
            vec![
                "1.0.0-awesome-feature.yml",
                "1.0.0-fix-everything-from-beta.yaml",
                "2.0.0-fix-previous-release.yml",
                "2.0.1-fix-previous-release.yml",
            ],
        );

        // test
        let result = collect_entry_files(temp_dir).unwrap();
        let expected: Vec<String> = vec![
            String::from(temp_file_path(temp_dir, "1.0.0-awesome-feature.yml")),
            String::from(temp_file_path(
                temp_dir,
                "1.0.0-fix-everything-from-beta.yaml",
            )),
            String::from(temp_file_path(temp_dir, "2.0.0-fix-previous-release.yml")),
            String::from(temp_file_path(temp_dir, "2.0.1-fix-previous-release.yml")),
        ];

        assert_eq!(
            result, expected,
            "Should find files adhering to change naming scheme"
        );

        // cleanup
        cleanup_temp_dir(
            temp_dir,
            vec![
                "1.0.0-awesome-feature.yml",
                "1.0.0-fix-everything-from-beta.yaml",
                "2.0.0-fix-previous-release.yml",
                "2.0.1-fix-previous-release.yml",
            ],
        );
    }

    #[test]
    fn should_ignore_irrelevant_files() {
        let temp_dir = "/tmp/changelog_utils_collect_should_ignore_irrelevant";
        setup_temp_dir(
            temp_dir,
            vec!["something-else.txt", "irrelevant.md", "foo.bar"],
        );

        // test
        let result = collect_entry_files(temp_dir).unwrap();
        let expected: Vec<String> = Vec::new();
        assert_eq!(result, expected, "Should not find any relevant file");

        cleanup_temp_dir(
            temp_dir,
            vec!["something-else.txt", "irrelevant.md", "foo.bar"],
        );
    }
}

#[cfg(test)]
mod test_filename_matches_meta_scheme {
    use super::*;

    #[test]
    fn should_recognize_both_yaml_and_yml() {
        assert!(filename_matches_meta_scheme("meta.yaml"));
        assert!(filename_matches_meta_scheme("meta.yml"));
    }

    #[test]
    fn should_recognize_invalid_filenames() {
        assert!(!filename_matches_meta_scheme("foo.yml"));
        assert!(!filename_matches_meta_scheme("bar.yml"));
        assert!(!filename_matches_meta_scheme("foo_meta.yaml"));
        assert!(!filename_matches_meta_scheme("meta_bar.yaml"));
    }
}
