use serde::Deserialize;

#[derive(Deserialize, Debug, PartialEq, Clone)]
#[serde(rename_all = "lowercase")]
pub enum ChangeType {
    Added,
    Changed,
    Fixed,
    Removed,
}

impl ChangeType {
    pub fn variants() -> Vec<Self> {
        //! for now we'll have to manually create this to define which variants appear in generated
        //! markdown. Considering the number of possible change types, I find this acceptable.
        //! See https://stackoverflow.com/questions/21371534/in-rust-is-there-a-way-to-iterate-through-the-values-of-an-enum
        //! Another possible solution would be to use a crate generating Enum iterators, but the
        //! one I tried requires Rust nightly.
        vec![
            ChangeType::Added,
            ChangeType::Changed,
            ChangeType::Fixed,
            ChangeType::Removed,
        ]
    }
}

impl std::fmt::Display for ChangeType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let display = match self {
            ChangeType::Added => "Added",
            ChangeType::Changed => "Changed",
            ChangeType::Fixed => "Fixed",
            ChangeType::Removed => "Removed",
        };

        write!(f, "{}", display)
    }
}
