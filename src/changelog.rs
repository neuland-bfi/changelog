use std::cmp::Ordering;
use std::collections::HashMap;
use std::fs::{read_dir, read_to_string};

use semver::Version;
use serde::Deserialize;
use serde_yaml;
use serde_yaml::Error as YamlError;

use crate::error::ChangeError;
use crate::types::ChangeType;
use crate::utils::{collect_entry_files, filename_matches_meta_scheme, version_from_filename};

#[derive(Deserialize, Debug, PartialEq, Clone)]
pub struct Change {
    title: String,
    #[serde(alias = "type")]
    change_type: ChangeType,
    merge_request: Option<u64>,
    issue: Option<u64>,
}

impl Change {
    fn from_yaml(yaml: &str) -> Result<Self, YamlError> {
        serde_yaml::from_str(yaml)
    }

    fn from_file(file_name: &str) -> Result<Self, ChangeError> {
        let file_content = read_to_string(file_name)?;
        Ok(Self::from_yaml(&file_content)?)
    }

    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn issue(&self) -> Option<u64> {
        self.issue
    }

    pub fn merge_request(&self) -> Option<u64> {
        self.merge_request
    }

    pub fn change_type(&self) -> &ChangeType {
        &self.change_type
    }

    #[cfg(test)]
    pub fn new(
        title: &str,
        change_type: ChangeType,
        issue: Option<u64>,
        merge_request: Option<u64>,
    ) -> Self {
        Change {
            title: String::from(title),
            change_type,
            merge_request,
            issue,
        }
    }
}

#[derive(Debug)]
pub struct Release {
    changes: Vec<Change>,
    version: Option<Version>,
}

impl Release {
    pub fn new(changes: Vec<Change>, version: Option<Version>) -> Self {
        Release { changes, version }
    }

    pub fn version(&self) -> &Option<Version> {
        &self.version
    }

    pub fn changes(&self) -> &Vec<Change> {
        &self.changes
    }

    pub fn change_types(&self) -> Vec<ChangeType> {
        let mut change_types: Vec<ChangeType> = Vec::new();
        for ct in ChangeType::variants() {
            let used_in_release: bool = !self
                .changes
                .iter()
                .filter(|c| c.change_type == ct)
                .collect::<Vec<&Change>>()
                .is_empty();
            if used_in_release {
                change_types.push(ct);
            }
        }
        change_types
    }
}

impl Ord for Release {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.version.is_some() && other.version.is_some() {
            self.version.cmp(&other.version)
        } else if self.version.is_none() && other.version.is_some() {
            // larger version = newer => unreleased at the top
            Ordering::Greater
        } else {
            Ordering::Less
        }
    }
}
impl PartialOrd for Release {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl PartialEq for Release {
    fn eq(&self, other: &Self) -> bool {
        self.version == other.version
    }
}
impl Eq for Release {}

#[derive(Deserialize, Debug, PartialEq)]
pub struct Meta {
    header: String,
}

impl Meta {
    fn from_yaml(yaml: &str) -> Result<Self, YamlError> {
        serde_yaml::from_str(yaml)
    }

    fn from_file(file_name: &str) -> Result<Self, ChangeError> {
        match read_to_string(&file_name) {
            Ok(s) => Ok(Self::from_yaml(&s)?),
            Err(_) => Ok(Meta::new(String::new())),
        }
    }

    fn from_dir(dir_path: &str) -> Result<Self, ChangeError> {
        for file in read_dir(dir_path)? {
            let file = file?;
            if file.path().is_dir() {
                continue;
            }
            match file.path().to_str() {
                Some(s) => {
                    if filename_matches_meta_scheme(file.file_name().to_str().unwrap_or("")) {
                        return Meta::from_file(s);
                    }
                }
                None => continue,
            }
        }
        Ok(Meta::new(String::new()))
    }

    pub fn new(header: String) -> Self {
        Meta { header }
    }

    pub fn header(&self) -> &str {
        &self.header
    }

    pub fn has_header(&self) -> bool {
        !self.header.is_empty()
    }
}

#[derive(Debug, PartialEq)]
pub struct Changelog {
    meta: Meta,
    releases: Vec<Release>,
}
impl Changelog {
    pub fn from_entries_dir(directory: &str) -> Result<Self, ChangeError> {
        let meta = Meta::from_dir(directory)?;
        let mut releases: HashMap<Option<Version>, Vec<Change>> = HashMap::new();
        for entry_file in collect_entry_files(directory)? {
            let version: Option<Version> = version_from_filename(&entry_file)?;
            releases
                .entry(version)
                .or_insert(Vec::new())
                .push(Change::from_file(&entry_file)?);
        }
        let mut releases: Vec<Release> = releases
            .into_iter()
            .map(|(version, changes)| Release::new(changes.to_vec(), version))
            .collect();
        releases.sort();
        releases.reverse();
        Ok(Changelog { meta, releases })
    }

    pub fn meta(&self) -> &Meta {
        &self.meta
    }

    pub fn releases(&self) -> &Vec<Release> {
        &self.releases
    }

    #[cfg(test)]
    pub fn new(meta: Meta, releases: Vec<Release>) -> Self {
        Changelog { meta, releases }
    }
}

#[cfg(test)]
mod test_change_from_yaml {
    use super::*;

    #[test]
    fn should_have_correct_mandatory_fields() {
        let yaml = r#"---
title: Awesome feature
type: added
"#;
        let change = Change::from_yaml(&yaml).unwrap();

        assert_eq!(change.title, String::from("Awesome feature"));
        assert_eq!(change.change_type, ChangeType::Added);
    }

    #[test]
    fn should_have_some_for_optional_fields_if_given() {
        let yaml = r#"---
title: Awesome feature
type: added
merge_request: 42
issue: 1337
"#;
        let change = Change::from_yaml(&yaml).unwrap();

        assert_eq!(change.merge_request, Some(42));
        assert_eq!(change.issue, Some(1337));
    }

    #[test]
    fn should_have_none_for_merge_request_if_omitted() {
        let yaml = r#"---
title: Awesome feature
type: added
issue: 42
"#;
        let change = Change::from_yaml(&yaml).unwrap();

        assert!(change.merge_request.is_none());
    }

    #[test]
    fn should_have_none_for_issue_if_omitted() {
        let yaml = r#"---
title: Awesome feature
type: added
merge_request: 42
"#;
        let change = Change::from_yaml(&yaml).unwrap();

        assert!(change.issue.is_none());
    }

    #[test]
    fn should_not_read_yaml_with_missing_mandatory_title() {
        let yaml = r#"---
type: added
"#;
        let change = Change::from_yaml(&yaml);

        assert!(change.is_err());
    }

    #[test]
    fn should_not_read_yaml_with_missing_mandatory_type() {
        let yaml = r#"---
title: Awesome feature
"#;
        let change = Change::from_yaml(&yaml);

        assert!(change.is_err());
    }
}

#[cfg(test)]
mod test_release_change_types {
    use super::*;

    #[test]
    fn should_contain_changetypes_used_in_release() {
        let release = Release::new(
            vec![
                Change::new("Add thing", ChangeType::Added, None, None),
                Change::new("Remove thing", ChangeType::Removed, None, None),
                Change::new("Change thing", ChangeType::Changed, None, None),
                Change::new("Fixed thing", ChangeType::Fixed, None, None),
            ],
            Some(Version::new(1, 0, 0)),
        );
        // this has to be ordered as defined in the enum
        let expected = vec![
            ChangeType::Added,
            ChangeType::Changed,
            ChangeType::Fixed,
            ChangeType::Removed,
        ];

        assert_eq!(
            release.change_types(),
            expected,
            "Should contain all used change types"
        );
    }

    #[test]
    fn should_not_contain_changetypes_not_used_in_release() {
        let release = Release::new(
            vec![Change::new("Add thing", ChangeType::Added, None, None)],
            Some(Version::new(1, 0, 0)),
        );
        let expected = vec![ChangeType::Added];

        assert_eq!(
            release.change_types(),
            expected,
            "Should contain only used change types"
        );
    }
}

#[cfg(test)]
mod test_release_ordering {
    use super::*;

    #[test]
    fn should_order_major_release_as_larger_than_smaller_major() {
        assert!(
            Release::new(Vec::new(), Some(Version::new(2, 0, 0)))
                > Release::new(Vec::new(), Some(Version::new(1, 0, 0)))
        );
        assert!(
            Release::new(Vec::new(), Some(Version::new(3, 0, 0)))
                > Release::new(Vec::new(), Some(Version::new(1, 0, 0)))
        );
        assert!(
            Release::new(Vec::new(), Some(Version::new(2, 1, 0)))
                > Release::new(Vec::new(), Some(Version::new(1, 42, 0)))
        );
    }

    #[test]
    fn should_order_minor_release_as_larger_than_major() {
        assert!(
            Release::new(Vec::new(), Some(Version::new(1, 1, 0)))
                > Release::new(Vec::new(), Some(Version::new(1, 0, 0)))
        );
        assert!(
            Release::new(Vec::new(), Some(Version::new(1, 9, 0)))
                > Release::new(Vec::new(), Some(Version::new(1, 0, 0)))
        );
        assert!(
            Release::new(Vec::new(), Some(Version::new(42, 2, 0)))
                > Release::new(Vec::new(), Some(Version::new(42, 0, 0)))
        );
    }

    #[test]
    fn should_order_minor_release_as_larger_than_smaller_minor() {
        assert!(
            Release::new(Vec::new(), Some(Version::new(1, 10, 0)))
                > Release::new(Vec::new(), Some(Version::new(1, 0, 0)))
        );
        assert!(
            Release::new(Vec::new(), Some(Version::new(1, 9, 0)))
                > Release::new(Vec::new(), Some(Version::new(1, 4, 0)))
        );
        assert!(
            Release::new(Vec::new(), Some(Version::new(1, 4, 0)))
                > Release::new(Vec::new(), Some(Version::new(1, 0, 0)))
        );
    }

    #[test]
    fn should_order_patch_release_as_larger_than_minor() {
        assert!(
            Release::new(Vec::new(), Some(Version::new(1, 0, 6)))
                > Release::new(Vec::new(), Some(Version::new(1, 0, 0)))
        );
        assert!(
            Release::new(Vec::new(), Some(Version::new(1, 10, 2)))
                > Release::new(Vec::new(), Some(Version::new(1, 10, 1)))
        );
        assert!(
            Release::new(Vec::new(), Some(Version::new(1, 9, 1)))
                > Release::new(Vec::new(), Some(Version::new(1, 9, 0)))
        );
    }

    #[test]
    fn should_order_patch_release_as_larger_than_smaller_path() {
        assert!(
            Release::new(Vec::new(), Some(Version::new(1, 14, 7)))
                > Release::new(Vec::new(), Some(Version::new(1, 14, 1)))
        );
        assert!(
            Release::new(Vec::new(), Some(Version::new(1, 10, 20)))
                > Release::new(Vec::new(), Some(Version::new(1, 10, 10)))
        );
    }
}

#[cfg(test)]
mod test_from_file_for_change {
    use super::*;
    use std::fs::{remove_file, write};

    #[test]
    fn should_return_not_found_for_none_existing_path() {
        let path: &str = "invalid-path";
        let result = Change::from_file(path);
        assert_eq!(
            result,
            Err(ChangeError::NotFound),
            "Should return error since the path does not exist"
        );
    }

    #[test]
    fn should_return_change_for_valid_path_and_valid_yaml_format() {
        let to_test_path: String =
            "/tmp/changelog_test_should_return_change_for_valid_path_and_valid_yaml_format.yaml"
                .to_string();
        let yaml_data: &str = r#"title: Test title
type: added
issue: 1
merge_request: 2"#;
        write(&to_test_path, yaml_data).unwrap();

        let result_change = Change::from_file(&to_test_path).unwrap();
        let expected_change = Change::new("Test title", ChangeType::Added, Some(1), Some(2));

        assert_eq!(
            expected_change, result_change,
            "Change should match file content!"
        );

        // cleanup
        remove_file(&to_test_path).unwrap();
    }
}

#[cfg(test)]
mod test_changelog_from_entries_dir {
    use std::fs::write;

    use super::*;
    use crate::utils::test_utils::{cleanup_temp_dir, setup_temp_dir, temp_file_path};

    #[test]
    fn should_generate_changelog_with_empty_meta_from_valid_entry_files_and_nonexisting_meta_file()
    {
        // setup temp test files
        let temp_dir = "/tmp/changelog_should_generate_changelog_with_empty_meta";
        setup_temp_dir(temp_dir, Vec::new()); // empty files, because here the content matters... see following
        write(
            temp_file_path(temp_dir, "1.0.0-awesome-feature.yml"),
            r#"---
title: Awesome feature
type: added
"#,
        )
        .unwrap();
        write(
            temp_file_path(temp_dir, "1.0.1-fix-devastating-bug.yml"),
            r#"---
title: Fix devastating bug
type: fixed
"#,
        )
        .unwrap();

        // test
        let result = Changelog::from_entries_dir(temp_dir).unwrap();
        let expected = Changelog::new(
            Meta::new(String::new()),
            vec![
                Release::new(
                    vec![Change::new(
                        "Fix devastating bug",
                        ChangeType::Fixed,
                        None,
                        None,
                    )],
                    Some(Version::new(1, 0, 1)),
                ),
                Release::new(
                    vec![Change::new(
                        "Awesome feature",
                        ChangeType::Added,
                        None,
                        None,
                    )],
                    Some(Version::new(1, 0, 0)),
                ),
            ],
        );

        assert_eq!(
            result, expected,
            "Should collect changes into a properly formatted changelog with empty meta"
        );

        // cleanup
        cleanup_temp_dir(
            temp_dir,
            vec!["1.0.0-awesome-feature.yml", "1.0.1-fix-devastating-bug.yml"],
        );
    }

    #[test]
    fn should_generate_changelog_with_matching_meta_from_valid_entry_files_and_valid_meta_file() {
        // setup temp test files
        let temp_dir = "/tmp/changelog_should_generate_changelog_with_matching_meta";
        setup_temp_dir(temp_dir, Vec::new()); // empty files, because here the content matters... see following
        write(
            temp_file_path(temp_dir, "meta.yaml"),
            r#"---
header: |
        Some description here.
        Yet another meta line."#,
        )
        .unwrap();
        write(
            temp_file_path(temp_dir, "1.0.0-awesome-feature.yml"),
            r#"---
title: Awesome feature
type: added
"#,
        )
        .unwrap();
        write(
            temp_file_path(temp_dir, "1.0.1-fix-devastating-bug.yml"),
            r#"---
title: Fix devastating bug
type: fixed
"#,
        )
        .unwrap();

        // test
        let result = Changelog::from_entries_dir(temp_dir).unwrap();
        let expected = Changelog::new(
            Meta::new(String::from(
                r#"Some description here.
Yet another meta line."#,
            )),
            vec![
                Release::new(
                    vec![Change::new(
                        "Fix devastating bug",
                        ChangeType::Fixed,
                        None,
                        None,
                    )],
                    Some(Version::new(1, 0, 1)),
                ),
                Release::new(
                    vec![Change::new(
                        "Awesome feature",
                        ChangeType::Added,
                        None,
                        None,
                    )],
                    Some(Version::new(1, 0, 0)),
                ),
            ],
        );

        assert_eq!(
            result, expected,
            "Should collect changes into a properly formatted changelog and meta"
        );

        // cleanup
        cleanup_temp_dir(
            temp_dir,
            vec![
                "1.0.0-awesome-feature.yml",
                "1.0.1-fix-devastating-bug.yml",
                "meta.yaml",
            ],
        );
    }
}

#[cfg(test)]
mod test_from_file_for_meta {
    use super::*;
    use std::fs::{remove_file, write};

    #[test]
    fn should_return_meta_with_identical_content_for_valid_meta_file() {
        let to_test_path: String =
            "/tmp/changelog_test_should_return_meta_with_identical_content_for_valid_meta_file.yaml"
                .to_string();

        let yaml_data: &str = r#"---
header: |
        Some description.
        Another newline.
        etc.."#;
        write(&to_test_path, yaml_data).unwrap();

        let result_meta = Meta::from_file(&to_test_path).unwrap();
        let expected_meta = Meta::new(
            r#"Some description.
Another newline.
etc.."#
                .to_string(),
        );

        assert_eq!(
            expected_meta, result_meta,
            "Meta should match file content!"
        );

        // cleanup
        remove_file(&to_test_path).unwrap();
    }

    #[test]
    fn should_yield_error_for_invalid_yaml_format() {
        let to_test_path: String =
            "/tmp/changelog_test_should_yield_error_for_invalid_yaml_format.yaml".to_string();

        let invalid_yaml: &str = "this is invalid yaml format";
        write(&to_test_path, invalid_yaml).unwrap();

        let meta = Meta::from_file(&to_test_path);

        assert!(meta.is_err(), "Should yield YamlError!");

        // cleanup
        remove_file(&to_test_path).unwrap();
    }

    #[test]
    fn should_return_meta_with_empty_content_for_nonexisting_path() {
        let to_test_path: String = "invalid_path".to_string();

        let result_meta = Meta::from_file(&to_test_path).unwrap();
        let expected_meta = Meta::new(String::from(""));

        assert_eq!(expected_meta, result_meta, "Meta should be empty!");
    }
}

#[cfg(test)]
mod test_from_dir_for_meta {
    use super::*;
    use crate::utils::test_utils::{cleanup_temp_dir, setup_temp_dir, temp_file_path};
    use std::fs::write;

    #[test]
    fn should_return_meta_with_identical_content_for_existing_and_valid_file_in_a_given_directory()
    {
        let to_test_dir = "/tmp/changelog_should_return_meta_with_identical_content";
        let meta_filename = "meta.yaml";
        setup_temp_dir(to_test_dir, vec![meta_filename]);
        let yaml_data: &str = r#"---
header: |
        Some description.
        Another newline.
        etc.."#;
        write(temp_file_path(to_test_dir, meta_filename), yaml_data).unwrap();

        let result_meta = Meta::from_dir(to_test_dir).unwrap();
        let expected_meta = Meta::new(
            r#"Some description.
Another newline.
etc.."#
                .to_string(),
        );

        assert_eq!(
            expected_meta, result_meta,
            "Meta should match file content!"
        );

        // cleanup
        cleanup_temp_dir(to_test_dir, vec![meta_filename]);
    }

    #[test]
    fn should_yield_error_for_existing_and_invalid_file_format_in_a_given_directory() {
        let to_test_dir = "/tmp/changelog_should_yield_error";
        let meta_filename = "meta.yaml";
        setup_temp_dir(to_test_dir, vec![meta_filename]);
        let yaml_data: &str = r"invalid yaml format";
        write(temp_file_path(to_test_dir, meta_filename), yaml_data).unwrap();

        let meta = Meta::from_dir(to_test_dir);

        assert!(meta.is_err(), "Should yield YamlError!");

        // cleanup
        cleanup_temp_dir(to_test_dir, vec![meta_filename]);
    }

    #[test]
    fn should_return_meta_with_empty_content_for_nonexisting_meta_file_in_a_given_directory() {
        let to_test_dir = "/tmp/changelog_should_return_meta_with_empty_content";
        setup_temp_dir(to_test_dir, vec![]);

        let result_meta = Meta::from_dir(to_test_dir).unwrap();
        let expected_meta = Meta::new(String::from(""));

        assert_eq!(expected_meta, result_meta, "Meta should be empty!");

        // cleanup
        cleanup_temp_dir(to_test_dir, vec![]);
    }
}

#[cfg(test)]
mod test_has_header_for_meta {
    use super::*;

    #[test]
    fn should_return_true_for_nonempty_header() {
        let meta = Meta::new(String::from("Not empty header"));
        assert!(meta.has_header());
    }

    #[test]
    fn should_return_false_for_empty_header() {
        let meta = Meta::new(String::new());
        assert!(!meta.has_header());
    }
}
