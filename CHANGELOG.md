# Changelog
All significant changes in this project are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
This file is generated from the change files in `changelog/`.
Add new changes there and don't edit this file directly as all manual changes will be overwritten.


## 1.0.2
### Fixed
- Fix random order when unreleased changes are included (#16, !13)

## 1.0.1
### Added
- Add real integration tests using a separate tests crate (#11, !12)

### Changed
- Cleanup code and divide into multiple modules (#11, !12)

## 1.0.0
### Added
- Collect changelog entries from yaml files in a given directory (#1, !5)
- Implement generating change from file (#6, !4)
- Include meta files in final Markdown (#7, !9)
- Implement generating markdown for changes (#4, !2)
- Implement generating markdown for changelog (#2, !6)
- Implement generating markdown for releases (#5, !3)
- Setup basic structure (#3, !1)
- Support unreleased changes not yet associated with a numbered release (#9, !11)

### Changed
- Omit empty changetype sections in the markdown for a release (#12, !8)
- Remove empty lines in a release's markdown, between the release heading and the first change type section (#8, !10)