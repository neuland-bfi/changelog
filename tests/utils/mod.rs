//! Utility functions to be used in tests
use std::fs::{create_dir_all, remove_dir, remove_file, write};

pub fn setup_temp_dir(directory: &str, files: Vec<(&str, &str)>) {
    create_dir_all(directory).unwrap();
    for (file, content) in files {
        write(temp_file_path(directory, file), content).unwrap();
    }
}

pub fn cleanup_temp_dir(directory: &str, files: Vec<&str>) {
    for file in files {
        remove_file(temp_file_path(directory, file)).unwrap();
    }
    remove_dir(directory).unwrap();
}

pub fn temp_file_path(directory: &str, basename: &str) -> String {
    format!("{}/{}", directory, basename)
}
