mod utils;
use utils::{cleanup_temp_dir, setup_temp_dir};

use changelog::{Changelog, Markdown};

#[test]
fn should_generate_changelog_from_directory() {
    // setup temp test files
    let temp_dir = "/tmp/changelog_should_generate_changelog_from_directory";
    setup_temp_dir(
        temp_dir,
        vec![
            (
                "0.1.0-beta-feature.yml",
                r#"---
title: Beta feature
type: added"#,
            ),
            (
                "1.0.0-awesome-feature.yml",
                r#"---
title: Awesome feature
type: added"#,
            ),
            (
                "1.0.0-fix-devastating-bug.yml",
                r#"---
title: Fix bug
type: fixed"#,
            ),
            (
                "1.1.0-change-beta-behavior.yml",
                r#"---
title: Change behavior of beta feature
type: changed"#,
            ),
            (
                "1.1.0-remove-feature.yml",
                r#"---
title: Remove thing
type: removed"#,
            ),
        ],
    );

    let expected_markdown = r#"# Changelog
## 1.1.0
### Changed
- Change behavior of beta feature

### Removed
- Remove thing

## 1.0.0
### Added
- Awesome feature

### Fixed
- Fix bug

## 0.1.0
### Added
- Beta feature"#;

    let changelog = Changelog::from_entries_dir(temp_dir).unwrap();
    assert_eq!(
        expected_markdown,
        changelog.markdown(),
        "Should generate expected markdown as specified in entry files"
    );

    // cleanup
    cleanup_temp_dir(
        temp_dir,
        vec![
            "0.1.0-beta-feature.yml",
            "1.0.0-awesome-feature.yml",
            "1.0.0-fix-devastating-bug.yml",
            "1.1.0-change-beta-behavior.yml",
            "1.1.0-remove-feature.yml",
        ],
    );
}

#[test]
fn should_generate_changelog_from_directory_with_heading_from_meta_file() {
    // setup temp test files
    let temp_dir =
        "/tmp/changelog_should_generate_changelog_from_directory_with_heading_from_meta_file";
    setup_temp_dir(
        temp_dir,
        vec![
            (
                "0.1.0-beta-feature.yml",
                r#"---
title: Beta feature
type: added"#,
            ),
            (
                "1.0.0-awesome-feature.yml",
                r#"---
title: Awesome feature
type: added"#,
            ),
            (
                "1.0.0-fix-devastating-bug.yml",
                r#"---
title: Fix bug
type: fixed"#,
            ),
            (
                "1.1.0-change-beta-behavior.yml",
                r#"---
title: Change behavior of beta feature
type: changed"#,
            ),
            (
                "1.1.0-remove-feature.yml",
                r#"---
title: Remove thing
type: removed"#,
            ),
            (
                "meta.yml",
                r#"---
header: |
    Test header for this markdown.
    Even with linebreaks."#,
            ),
        ],
    );

    let expected_markdown = r#"# Changelog
Test header for this markdown.
Even with linebreaks.

## 1.1.0
### Changed
- Change behavior of beta feature

### Removed
- Remove thing

## 1.0.0
### Added
- Awesome feature

### Fixed
- Fix bug

## 0.1.0
### Added
- Beta feature"#;

    let changelog = Changelog::from_entries_dir(temp_dir).unwrap();
    assert_eq!(
        expected_markdown,
        changelog.markdown(),
        "Should generate expected markdown as specified in entry files"
    );

    // cleanup
    cleanup_temp_dir(
        temp_dir,
        vec![
            "0.1.0-beta-feature.yml",
            "1.0.0-awesome-feature.yml",
            "1.0.0-fix-devastating-bug.yml",
            "1.1.0-change-beta-behavior.yml",
            "1.1.0-remove-feature.yml",
            "meta.yml",
        ],
    );
}

#[test]
fn should_ignore_non_changelog_files_when_generating_changelog() {
    // setup temp test files
    let temp_dir = "/tmp/changelog_should_ignore_non_changelog_files_when_generating_changelog";
    setup_temp_dir(
        temp_dir,
        vec![
            (
                "0.1.0-beta-feature.yml",
                r#"---
title: Beta feature
type: added"#,
            ),
            (
                "1.0.0-awesome-feature.yml",
                r#"---
title: Awesome feature
type: added"#,
            ),
            (
                "1.0.0-fix-devastating-bug.yml",
                r#"---
title: Fix bug
type: fixed"#,
            ),
            (
                "1.1.0-change-beta-behavior.yml",
                r#"---
title: Change behavior of beta feature
type: changed"#,
            ),
            (
                "1.1.0-remove-feature.yml",
                r#"---
title: Remove thing
type: removed"#,
            ),
            ("foo.yml", "content irrelevant"),
        ],
    );

    let expected_markdown = r#"# Changelog
## 1.1.0
### Changed
- Change behavior of beta feature

### Removed
- Remove thing

## 1.0.0
### Added
- Awesome feature

### Fixed
- Fix bug

## 0.1.0
### Added
- Beta feature"#;

    let changelog = Changelog::from_entries_dir(temp_dir).unwrap();
    assert_eq!(
        expected_markdown,
        changelog.markdown(),
        "Should generate expected markdown as specified in entry files"
    );

    // cleanup
    cleanup_temp_dir(
        temp_dir,
        vec![
            "0.1.0-beta-feature.yml",
            "1.0.0-awesome-feature.yml",
            "1.0.0-fix-devastating-bug.yml",
            "1.1.0-change-beta-behavior.yml",
            "1.1.0-remove-feature.yml",
            "foo.yml",
        ],
    );
}
